import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './component/App';
import TestBitly from './component/TestBitlySL'
import registerServiceWorker from './registerServiceWorker';
import TestCallingAPI from './component/TestCallingAPI'

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
