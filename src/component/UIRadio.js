import React, { Component } from 'react';
import PropTypes from 'prop-types';

class UIRadio extends React.Component {
    render() {
        return (
            <div className="form-check">
                <input required={this.props.isRequired} className="form-check-input" type="radio" name="gridRadios" id={this.props.idInput} onChange={this.props.handleFunc} value={this.props.valueInput} checked={this.props.isChecked}/>
                <label className="form-check-label">
                    {this.props.valueInput}
            </label>
            </div>
        )
    }
}

UIRadio.prototypes = {
    idInput : PropTypes.string.isRequired,
    valueInput : PropTypes.string.isRequired,
    handleFunc : PropTypes.func.isRequired,
    isChecked : PropTypes.bool,
    isRequired : PropTypes.bool
}

export default UIRadio