import React, { Component } from 'react';
import PropTypes from 'prop-types';

class UICheckBox extends React.Component {
    render() {
        return (
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value={this.props.valueInput} name={this.props.nameInput} onChange={this.props.handleFunc}/>
                <label className="form-check-label">
                    {this.props.displayValue}
                </label>
            </div>
        )
    }
}

UICheckBox.prototypes = {
    nameInput: PropTypes.string.isRequired,
    valueInput: PropTypes.string.isRequired,
    handleFunc: PropTypes.func.isRequired,
    displayValue: PropTypes.string.isRequired
}

export default UICheckBox