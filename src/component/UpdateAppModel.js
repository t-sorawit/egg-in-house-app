import React, { Component } from 'react';
import withAuthorization from './withAuthorization';
import { auth, db } from '../firebase';
import icon from '../icon.png'
import SingleInput from './SingleInput';
import firebase from 'firebase';
import ProgressBar from './ProgressBar';
import LaunchModal from './LaunchModal';
import { Route, Redirect } from 'react-router';
import { LIST_APP } from '../constants/routes';
import UIRadio from './UIRadio';
import UICheckBox from './UICheckBox';
import PropTypes from 'prop-types';
import SlideShow from './SlideShow'
class UpdateAppModel extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isUploading: false,
            isEditing: true,
            name: "",
            version: "",
            build: "",
            bundleId: "",
            iconFile: "",
            imagePreviewUrl: icon,
            ipaFileUrl: "",
            appFile: "",
            description: "desc",
            screenshots: { img1: "url" },
            timeStamp: undefined,
            lastest_timestamp: undefined,
            device: "iPhone",
            ipaFile: "",
            maniFestPlist: "",
            downloadProgress: 0,
            downloadState: 0,
            uploadFilename: "filename",
            success: false,
            screenshotsFile: undefined,
            screenshotFiles: undefined,
            scrObj: [],
            group_access: [],
            group_access_raw: {},
            isUploadIPA: false,
            screenshotss: {},
            isSHUpload: false,
            countSHFile: 0
        }

        this.handleAddIcon = this.handleAddIcon.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleBuildChange = this.handleBuildChange.bind(this)
        this.handleBundleIdChange = this.handleBundleIdChange.bind(this)
        this.handleVersionChange = this.handleVersionChange.bind(this)
        this.submitForm = this.submitForm.bind(this)
        this.handleIpaFile = this.handleIpaFile.bind(this)
        this.handleSaveDataToDB = this.handleSaveDataToDB.bind(this)
        this.saveIpaFileToStorage = this.saveIpaFileToStorage.bind(this)
        //this.handleWritePlistFile = this.handleWritePlistFile.bind(this)
        this.saveManiFestPlistToStorage = this.saveManiFestPlistToStorage.bind(this)

    }

    componentDidMount() {
        console.log("appKeyId", this.props.appKeyId)
        const appId = this.props.appKeyId
        const uid = auth.getUserCurrent()
        console.log("didmount", uid)
        var ts = this

        db.getUsers().then( function (snapshot) {
            console.log("XXXX", snapshot.val())
            let users = snapshot.val()
            ts.setState({
                uid: uid,
                group_access_raw: users
            })
        })

        db.getApplicationsByName(appId).then(function (data) {
            const appDetail = data.val()
            console.log("app detail", data.val())
            ts.setState({
                screenshotss: appDetail.screenshots,
                name: appDetail.name,
                version: appDetail.version,
                build: appDetail.build,
                bundleId: appDetail.bundleId,
                imagePreviewUrl: appDetail.appIconUrl
            })
        })

    }

    handleWritePlistFile = () => {
        var XMLWriter = require('xml-writer');
        var xw = new XMLWriter;
        xw.startDocument();
        xw.startElement('plist');
        xw.writeAttribute('version', "1.0");
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("items")
        xw.endElement()
        xw.startElement("array")
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("assets")
        xw.endElement()
        xw.startElement('array')
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("kind")
        xw.endElement()
        xw.startElement('string')
        xw.text("software-package")
        xw.endElement()
        xw.startElement('key')
        xw.text("url")
        xw.endElement()
        xw.startElement('string')
        xw.text(this.state.ipaFileUrl)   // <--- input url for .ipa file
        xw.endElement()
        xw.endElement()
        xw.endElement()
        xw.startElement('key')
        xw.text("metadata")
        xw.endElement()

        xw.startElement("dict")
        xw.startElement("key")
        xw.text("bundle-identifier")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.bundleId)  // <--- input bundle id
        xw.endElement()
        xw.startElement("key")
        xw.text("bundle-version")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.version) // <--- input bundle id
        xw.endElement()
        xw.startElement("key")
        xw.text("kind")
        xw.endElement()
        xw.startElement("string")
        xw.text("software")
        xw.endElement()
        xw.startElement("key")
        xw.text("subtitle")
        xw.endElement()
        xw.startElement("string")
        xw.text("subtitle here") // <--- input subtitle
        xw.endElement()
        xw.startElement("key")
        xw.text("title")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.name) // <--- input title here
        xw.endElement()
        xw.endDocument();
        this.setState({
            maniFestPlist: xw
        })
        this.saveManiFestPlistToStorage()
    }

    saveManiFestPlistToStorage = async () => {
        // SemiFinal step
        var thisS = this
        const isUpload = thisS.state.isUploadIPA
        if (isUpload) {
            var manifestFile = thisS.state.maniFestPlist
            var storage = firebase.storage();
            var storageRef = storage.ref();
            var imageRef = storageRef.child(thisS.state.name);
            var spaceRef = imageRef.child('ipa/manifest.plist');
            var file = new Blob([manifestFile.toString()], { type: 'application/octet-stream' });

            var uploadManifestRef = spaceRef.put(file).then(async (snapshot) => {
                //var downloadURL = snapshot.downloadURL;
                var longUrl = snapshot.downloadURL
                const API_KEY = "AIzaSyDR3vFkRx8HRc3OkT4y-zj8T2SQFfjfvfw"
                const DYNAMIC_LINK = "mhzw9.app.goo.gl"

                const URL_REQUEST = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`
                const headers = new Headers()
                console.log("QQQQ", URL_REQUEST)
                headers.append('Content-Type', 'application-json')
                const data = {
                    "dynamicLinkInfo": {
                      "dynamicLinkDomain": DYNAMIC_LINK,
                      "link": longUrl
                    },
                    "suffix": {
                      "option": "SHORT"
                    }
                }

                const options = {
                    method: 'POST',
                    headers,
                    body: JSON.stringify(data)
                }

                const request = new Request(URL_REQUEST, options)
                const response = await fetch(request)
                const status = await response.status

                if (status == 200) {
                    const jsonData = await response.json()
                    const shortenUrl = jsonData.shortLink
                    thisS.setState({
                      appFile: shortenUrl
                    })
                    console.log("shortURL", )
                    this.handleSaveDataToDB()
                    console.log("shortURL", shortenUrl)
                }


            })





        }
    }

    submitForm(event) {
        var thisS = this
        console.log("touch")
        thisS.setState({
            isUploading: true
        })
        this.saveScreenshotFiles();
        this.saveIpaFileToStorage();
        event.preventDefault();

    }

    handleClearForm(e) {
        e.preventDefault();
    }

    handleSaveDataToDB = () => {
        //Final step
        var dbb = firebase.database();
        var group_access_final = this.state.group_access
        if (group_access_final.length === 0) {
            group_access_final = { General: true }
        }

        var timeInterval = firebase.database.ServerValue.TIMESTAMP
        const isIpaUpload = this.state.isUploadIPA
        const isScreenshotUpload = this.state.isSHUpload
        if (isIpaUpload && isScreenshotUpload) {
            dbb.ref(`Applications/${this.props.appKeyId}`).update({
                name: this.state.name,
                appIconUrl: this.state.imagePreviewUrl,
                screenshots: this.state.scrObj,
                fileAppUrl: this.state.appFile,
                group_access: group_access_final,
                description: this.state.description,
                device: this.state.device,
                lastest_timestamp: timeInterval,
                version: this.state.version,
                build: this.state.build,
                bundleId: this.state.bundleId
            })
        } else if (isIpaUpload) {
            dbb.ref(`Applications/${this.props.appKeyId}`).update({
                name: this.state.name,
                appIconUrl: this.state.imagePreviewUrl,
                fileAppUrl: this.state.appFile,
                group_access: group_access_final,
                description: this.state.description,
                device: this.state.device,
                lastest_timestamp: timeInterval,
                version: this.state.version,
                build: this.state.build,
                bundleId: this.state.bundleId
            })
        } else if (isScreenshotUpload) {
            console.log("is screnen")
            console.log("screenshot xxxxxxxx", this.state.scrObj)
            const scr = this.state.scrObj
            var countFile = this.state.countSHFile
            var objSH = {}
            for (var i = 1; i <= countFile; i++) {
                objSH[`img${i}`] = scr[`img${i}`]
            }

            dbb.ref(`Applications/${this.props.appKeyId}`).update({
                name: this.state.name,
                appIconUrl: this.state.imagePreviewUrl,

                group_access: group_access_final,
                description: this.state.description,
                device: this.state.device,
                lastest_timestamp: timeInterval,
                version: this.state.version,
                build: this.state.build,
                bundleId: this.state.bundleId
            })
        } else {
            dbb.ref(`Applications/${this.props.appKeyId}`).update({
                name: this.state.name,
                appIconUrl: this.state.imagePreviewUrl,
                group_access: group_access_final,
                description: this.state.description,
                device: this.state.device,
                lastest_timestamp: timeInterval,
                version: this.state.version,
                build: this.state.build,
                bundleId: this.state.bundleId
            })
        }

        this.setState({
            success: true,
            isEditing: false
        })
    }

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleVersionChange(e) {
        this.setState({
            version: e.target.value
        });
    }

    handleBuildChange(e) {
        this.setState({
            build: e.target.value
        });
    }

    handleBundleIdChange(e) {
        this.setState({
            bundleId: e.target.value
        });
    }

    saveIpaFileToStorage() {

        var thisS = this
        const isUpload = thisS.state.isUploadIPA
        if (isUpload) {
            var storage = firebase.storage();
            var storageRef = storage.ref();
            var imageRef = storageRef.child(thisS.state.name);
            var fileIpaRaw = thisS.state.ipaFile
            var spaceRef = imageRef.child('ipa/app.ipa');
            var metadata = {
                contentType: 'application/octet-stream',
            }

            var x = spaceRef.put(fileIpaRaw, metadata);
            x.on('state_changed', function (snapshot) {
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('ipa file Upload is ' + progress + '% done');
                thisS.setState({
                    downloadProgress: progress
                })
            }, function (error) {
                // Handle unsuccessful uploads
            }, function () {
                var downloadURL = x.snapshot.downloadURL;
                thisS.setState({
                    ipaFileUrl: downloadURL
                })
                console.log("download", downloadURL)
                thisS.handleWritePlistFile()
            });
        }
    }

    handleIpaFile(e) {
        let file = e.target.files[0];
        this.setState({
            ipaFile: file,
            isUploadIPA: true
        })
    }

    handleScreenshotFile = (e) => {

        if (e.target.files.length > 5) {
            var dataFile = document.getElementById("screenshot").value
            alert("ไฟล์เยอะไปจ้า")
            document.getElementById("screenshot").value = ""
            this.setState({
                isSHUpload: false
            })
        } else if (e.target.files.length > 0 && e.target.files.length < 6) {

            this.setState({
                isSHUpload: true,
                screenshotFiles: e.target.files
            })
            console.log("screenshot state", this.state.screenshotFiles)
            console.log("screenshot", e.target.files)
        }
    }

    saveScreenshotFiles = (e) => {
        var files = this.state.screenshotFiles
        var metadata = {
            contentType: 'image/png',
        };
        var dbb = firebase.database();
        const isUpload = this.state.isSHUpload
        if (isUpload) {
            for (var i = 0; i < files.length; i++) {
                var imageFile = files[i]
                var fileName = `img${i + 1}`
                this.setState({
                    countSHFile: files.length
                })
                this.uploadFile(imageFile, fileName, result => {
                    if (result.progress) {
                        console.log("progress", result.progress)
                        return;
                    }

                    if (result.downloadURL && result.keyName) {
                        console.log("url", result.downloadURL)
                        var scrArr = this.state.scrObj
                        var scrArrs = {}
                        scrArr[result.keyName] = result.downloadURL
                        scrArrs[result.keyName] = result.downloadURL

                        dbb.ref(`Applications/${this.props.appKeyId}/screenshots`).update(scrArrs)
                        console.log("scr: ", this.state.scrObj)
                        return;
                    }

                    if (result.error) {
                        console.log(`error upload${i}`, result.error)
                    }
                });
            }
            this.handleSaveDataToDB()
        }
        console.log("sasas")
    }

    handleGroupChange = () => {
        var checkboxes = document.getElementsByName('group_access[]');
        var vals = []
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            if (checkboxes[i].checked) {
                vals[checkboxes[i].value] = true
                this.setState({
                    group_access: vals
                })
                console.log("checked", vals)
            }
        }
    }

    handleDevieChange = (e) => {
        const deviceModel = e.target.value
        console.log("device choose", deviceModel)
        this.setState({
            device: deviceModel
        })
    }

    async uploadFile(file, fileName, callback) {
        var thiss = this

        var storage = firebase.storage();
        const storageRef = storage.ref()
        const uploadTask = storageRef.child(`${thiss.state.name}/screenshot/${fileName}.png`).put(file)

        uploadTask.on('state_changed', snapshot => {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            callback({ progress });
        }, error => {
            callback({ error });
        }, () => {
            var downloadURL = uploadTask.snapshot.downloadURL;
            var keyName = fileName
            callback({ downloadURL, keyName });
        });
    }

    handleEditForm = (e) => {
        e.preventDefault()
        var isEdit = this.state.isEditing
        this.setState({
            isEditing: !isEdit
        })
    }

    handleAddIcon = (e) => {

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }
        reader.readAsDataURL(file)
    }

    handleAddIcons = (e) => {
        console.log("add icon")
        var thisS = this
        // let reader = new FileReader();
        let file = e.target.files[0];
        var storage = firebase.storage();
        var storageRef = storage.ref();
        var imageRef = storageRef.child(thisS.state.name);
        var spaceRef = imageRef.child('appIcon/appIcon.png');
        var metadata = {
            contentType: 'image/jpeg',
        };
        var x = spaceRef.put(file, metadata);

        x.on('state_changed', function (snapshot) {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            thisS.setState({
                downloadProgress: progress
            })

        }, function (error) {

        }, function () {
            var downloadURL = x.snapshot.downloadURL;
            thisS.setState({
                imagePreviewUrl: downloadURL
            })
            console.log("download", downloadURL)
        });
    }

    render() {
        let { imagePreviewUrl } = this.state;
        let _slides = this.state.screenshotss
        var scArr = []
        var usersValue = this.state.group_access_raw
        console.log(_slides)
        Object.keys(_slides).map(x => {
            var tmp = {}
            tmp["imgUrl"] = _slides[x]
            scArr.push(tmp)
        })

        console.log("scArr", scArr)

        return (
            <div>
                <h1>Update Application</h1>
                <hr />
                <div>
                    <button onClick={this.handleEditForm} className="btn btn-warning" hidden={!this.state.isEditing}>Update</button>&nbsp;
                    <button onClick={this.handleEditForm} className="btn btn-warning" hidden={this.state.isEditing}>Cancel</button>&nbsp;
                    <button className="btn btn-success" onClick={this.props.handleFunc} >Back</button>
                    <hr />
                    <div className="card-group" style={{ width: '96px', height: '96px' }}>
                        <img className="card-img-top" src={imagePreviewUrl} />
                    </div>
                    <hr />
                    <div hidden={this.state.isUploading}>
                        <form onSubmit={this.submitForm}>

                            <SingleInput
                                valueInput={this.state.name}
                                inputType={'text'}
                                title={'Name'}
                                name={'name'}
                                controlFunc={this.handleNameChange}
                                placeholder={'Type name here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.version}
                                inputType={'text'}
                                title={'Version'}
                                name={'version'}
                                controlFunc={this.handleVersionChange}
                                placeholder={'Type version here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.build}
                                inputType={'text'}
                                title={'Build'}
                                name={'build'}
                                controlFunc={this.handleBuildChange}
                                placeholder={'Type build here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.bundleId}
                                inputType={'text'}
                                title={'Bundle Id'}
                                name={'bundleId'}
                                controlFunc={this.handleBundleIdChange}
                                placeholder={'Type bund id here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.description}
                                inputType={'text'}
                                title={'Description'}
                                name={'descritpion'}
                                controlFunc={this.onDescriptionChange}
                                placeholder={'typing...'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                hiddenInput={this.state.isEditing}
                                acceptFile={'image/*'}
                                inputType={'file'}
                                title={'Icon'}
                                name={'icon'}
                                controlFunc={this.handleAddIcon}
                                multipleUpload={false}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                hiddenInput={this.state.isEditing}
                                acceptFile={'.ipa'}
                                inputType={'file'}
                                title={'.ipa file'}
                                name={'fileApp'}
                                controlFunc={this.handleIpaFile}

                                multipleUpload={false}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                hiddenInput={this.state.isEditing}
                                idInput={'screenshot'}
                                acceptFile={'image/*'}
                                inputType={'file'}
                                title={'Screenshot'}
                                name={'screenshot'}
                                controlFunc={this.handleScreenshotFile}

                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                hiddenInput={!this.state.isEditing}
                                valueInput={this.state.device}
                                idInput={'device'}
                                inputType={'text'}
                                title={'device'}
                                name={'device'}
                                required={'true'}
                                disabledInput={this.state.isEditing}
                            />

                            <div className="row" hidden={!this.state.isEditing} >
                                <SlideShow slides={scArr} />
                            </div>

                            <div className="form-group" hidden={this.state.isEditing}>
                                <div className="row">
                                    <legend className="col-form-label col-sm-2 pt-0">Device</legend>
                                    <div className="col-sm-10">
                                        <UIRadio
                                            isRequired={true}
                                            idInput={'device'}
                                            valueInput={'iPhone'}
                                            handleFunc={this.handleDevieChange}
                                        />
                                        <UIRadio
                                            idInput={'device'}
                                            valueInput={'iPad'}
                                            handleFunc={this.handleDevieChange}
                                        />
                                        <UIRadio
                                            idInput={'device'}
                                            valueInput={'Universal'}
                                            handleFunc={this.handleDevieChange}
                                        />
                                    </div>
                                </div>
                            </div>


                            <div className="form-group" hidden={this.state.isEditing}>

                                <div className="row">
                                    <legend className="col-form-label col-sm-2 pt-0">User Access</legend>
                                    <div className="col-sm-10">
                                        {
                                            Object.keys(this.state.group_access_raw).map(x => {
                                                return <UICheckBox
                                                    key={x}
                                                    nameInput={'group_access[]'}
                                                    valueInput={x}
                                                    displayValue={usersValue[x].name}
                                                    handleFunc={this.handleGroupChange}
                                                />
                                            })
                                        }
                                    </div>
                                </div>
                            </div>

                            <button className="btn btn-primary" hidden={this.state.isEditing} disabled={false} >Submit form</button>

                        </form>
                    </div>
                    <div hidden={this.state.isEditing}>
                        <ProgressBar
                            title={this.state.uploadFilename}
                            name={'uploadBar'}
                            downloadProgress={this.state.downloadProgress}
                        />

                        {
                            this.state.success == true &&
                            <LaunchModal
                                hidden={this.state.isEditing}
                                title={'Upload process'}
                                msg={'Success! >>' + this.state.appFile}
                            />
                        }
                    </div>
                </div>
            </div>
        )
    }
}

UpdateAppModel.propTypes = {
    appKeyId: PropTypes.string.isRequired,
    handleFunc: PropTypes.func
}

export default UpdateAppModel