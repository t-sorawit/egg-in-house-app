import React, { Component } from 'react';
import PropTypes from 'prop-types';


class UISlideImageCell extends React.Component {

    render() {
        console.log("screenshot item", this.props.srcImage)
        return (
            <div className="item">
                    <img className="d-block w-100" src={this.props.srcImage} alt="Third slide" />
            </div>
            
            

        )
    }

}

UISlideImageCell.propTypes = {
    srcImage: PropTypes.string
}

export default UISlideImageCell







