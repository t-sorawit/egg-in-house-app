
import React, { Component } from 'react';
import PropTypes from 'prop-types';


// 0a5df1b76ef8dd2b9f4fcdaf0b57d451d0ce0bb7
class TestBitly extends React.Component {
  constructor(props) {

    super(props)
    this.state = {
      longUrl: "",
      shortUrl: ""
    }
  }

  handleShortenLink = () => {
    var uri = this.state.longUrl
    console.log("url", uri)
    const BitlyClient = require('bitly');
    const bitly = BitlyClient('0a5df1b76ef8dd2b9f4fcdaf0b57d451d0ce0bb7');

    bitly.shorten(uri)
      .then(function (result) {
        console.log(result);
        console.log("data", result.data)
        console.log("url short",result.data.url)
        console.log("xxx")
      })
      .catch(function (error) {
        console.error(error);
      });

  }



  handleTxtUrlChange = (e) => {
    var url = e.target.value
    this.setState({
      longUrl: url
    })
    console.log("long", url)

  }

  render() {
    return (
      <div className="form-group">
        <input type="text" onChange={this.handleTxtUrlChange} />
        <button onClick={this.handleShortenLink} />
      </div>
    )
  }


}
export default TestBitly;  