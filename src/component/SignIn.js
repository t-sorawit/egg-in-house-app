
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { auth } from '../firebase';
import * as routes from '../constants/routes';

const SignInPage = ({ history }) =>
  <div>
    <h1>Sign In</h1>
    <SignInForm history={history} />
  </div>

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  displayName: '',
  error: null,
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  handleFBSignin = (event) => {
    const {
      email,
      displayName
    } = this.state;

    const {
      history
    } = this.props;

    auth.doSignInWithFacebook()
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
    event.preventDefault();
  }

  handleGLSignin = (event) => {
    const {
      email,
      displayName
    } = this.state;

    const {
      history
    } = this.props;

    auth.doSignInWithGoogle()
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
    event.preventDefault();
  }

  render() {
    const {
      email,
      displayName,
      error,
    } = this.state;

    return (
      <form className="form">
        
        <div className="form-group">
        <button className="btn btn-primary mb-2" onClick={this.handleFBSignin}>
          Sign In with Facebook
        </button>
        </div>
        <div className="form-group">
        <button className="btn btn-primary mb-2" onClick={this.handleGLSignin} >
          Sign In with Google
        </button>
        </div>
        { error && <p>{error.message}</p> }
      </form>
    );
  }
}

export default withRouter(SignInPage);

export {
  SignInForm
};