import React from 'react';
import PropTypes from 'prop-types';

class UIModal extends React.Component {
    render() {
        return (
            <div className="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{this.props.title}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                           {this.props.body}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-danger" onClick={this.props.handleFunc} data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

UIModal.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    handleFunc: PropTypes.func.isRequired
}

export default UIModal

