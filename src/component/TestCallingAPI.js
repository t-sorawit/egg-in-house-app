

//https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDR3vFkRx8HRc3OkT4y-zj8T2SQFfjfvfw&longDynamicLink=https://mhzw9.app.goo.gl/?link=https://hothardware.com/news/google-to-discontinue-googl-url-shortener-and-migrate-to-firebase-dynamic-links


import React, { Component } from 'react';
import PropTypes from 'prop-types';





class TestCallingAPI extends React.Component {
    constructor(props) {

        super(props)
        this.state = {
            longUrl: "",
            shortUrl: ""
        }

    }

    handleShortenLink = async () => {
        console.log(this.shortenPath())
    }

    shortenPath = async () => {
        var longUrl = this.state.longUrl
        const API_KEY = "AIzaSyDR3vFkRx8HRc3OkT4y-zj8T2SQFfjfvfw"
        const DYNAMIC_LINK = "https://mhzw9.app.goo.gl/"

        const URL_REQUEST = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`

        const headers = new Headers()
        headers.append('Content-Type', 'application/json')

        const data = {
            "longDynamicLink": `https://mhzw9.app.goo.gl/?link=${longUrl}`,
            "suffix": {
                "option": "SHORT"
            }
        }

        const options = {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }

        const request = new Request(URL_REQUEST, options)
        const response = await fetch(request)
        const status = await response.status
        const jsonRes = await response.json()
        if(status == 200) {
            console.log("shortURL", jsonRes.shortLink)
        }else {
            console.log(status)
        }
        
    }

    handleTxtUrlChange = (e) => {
        const longUrl = e.target.value
        this.setState({
            longUrl: longUrl
        })
    }

    render() {
        return (
            <div className="form-group">
                <input type="text" onChange={this.handleTxtUrlChange} />
                <button onClick={this.handleShortenLink} />
            </div>
        )
    }

}

export default TestCallingAPI;  