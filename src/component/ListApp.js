import React from 'react';
import withAuthorization from './withAuthorization';
import LaunchModal from './LaunchModal'
import { db, auth } from '../firebase'
import UpdateAppModel from './UpdateAppModel';
import firebase from 'firebase'
import UIModal from './UIModal';
import UpdateNewAppModel from './UpdateNewAppModel'

class ListAppPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isEditing: false,
      applicationItems: [],
      appKeyIdSelect: undefined
    }
  }

  componentDidMount() {
    this.fetchItem()
  }

  fetchItem = () => {
    var self  = this
    const userUid = auth.getUserCurrent()
    console.log("USER UID", userUid)
    var objApplications = {}

    var self = this
    firebase.database().ref('Users').child(userUid).child("yourApp").on('value', function (snapshot) {
      console.log("YOUR APPLICATION", snapshot.val())
      var applicationListItem = {}
      for (const key in snapshot.val()) {
        firebase.database().ref('Applications').child(key).once('value', function(snapshot){
          console.log("OUR APPLICATION", snapshot.val())
          applicationListItem[key] = snapshot.val()
          self.setState({
            applicationItems: applicationListItem
          })
        } )
      }

      
    })

    // firebase.database().ref('Users').child(userUid).child("yourApp").on('value', function (snapshot) {
    //   console.log("YOUR APPlication", snapshot.val())
    //   const yourApp = snapshot.val()
      
    //   for (const key in yourApp) {
    //     console.log("KEY",key)
    //     firebase.database().ref('Applications').child(key).on('value', function(snapshot){
    //       console.log("LOG", snapshot.val())
          
    //       objApplications[key] = snapshot.val()
    //       self.setState({
    //         applicationItems: objApplications
    //       })
    //     })
    //   }
    // })



    // firebase.database().ref('Applications').on('value', function (snapshot) {
    //   console.log("log data application", snapshot.val())
    //   const data = snapshot.val()
    //   console.log("key app", data)
    //   self.setState({
    //     applicationItems: data
    //   })

    // })
  }

  handleEdit = (e) => {
    const nameApp = e.target.value
    console.log("name app", nameApp)
    this.setState({
      isEditing: true,
      appKeyIdSelect: nameApp
    })
  }

  onBackPage = (e) => {
    this.setState({
      isEditing: false
    })
  }

  handleDelete = (e) => {
    const appID = this.state.selectedApp
    
    const uid = auth.getUserCurrent()

    //db.deleteOurApplication(appID, uid)
    db.deleteApplication(appID)
  }

  onClickDelete = (e) => {
    const nameApp = e.target.value
    this.setState({
      selectedApp: nameApp
    })
  }

  render() {

    if (!this.state.isEditing){

    const itemApplication = this.state.applicationItems
    if(itemApplication.length == 0) {
      console.log("NOT FOUND")
      return (
        <div className="container">
        <h2>List Applications</h2>

        <p>our applications: 0 item</p>
       
          </div>
      )
    }
    return (
      <div className="container">

        <UIModal
          title="Are you sure?"
          id="exampleModal"
          body="Delete this application!"
          handleFunc={this.handleDelete}
        />

        <h2>List Applications</h2>

        <p>list applications</p>
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Icon</th>
              <th>Name Applcation</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

    {
              Object.keys(this.state.applicationItems).map(item => {
                return <tr key={item}>
                  <td><img src={this.state.applicationItems[item].appIconUrl} width="32px" /></td>
                  <td>{this.state.applicationItems[item].name}</td>
                  <td>
                    <button className="btn btn-warning btn-sm" value={item} onClick={this.handleEdit}>Detail</button>&nbsp;
                  </td>
                </tr>
              })
            }
          </tbody>
        </table>
      </div>
    )
  }else {
    return(

      <UpdateNewAppModel
      appID= {this.state.appKeyIdSelect}
      />
    )
  }
}
}


const authCondition = (authUser) => !!authUser;
export default withAuthorization(authCondition)(ListAppPage);
