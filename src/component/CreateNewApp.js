import React, { Component } from 'react';
import withAuthorization from './withAuthorization';
import { auth, db } from '../firebase';
import icon from '../icon.png'
import SingleInput from './SingleInput';
import firebase from 'firebase';
import ProgressBar from './ProgressBar';
import LaunchModal from './LaunchModal';
import { LIST_APP } from '../constants/routes';
import UIRadio from './UIRadio';
import UICheckBox from './UICheckBox';

class CreateNewAppPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      uuid: "",
      uid: "",
      isUploading: false,
      name: "",
      version: "",
      build: "",
      bundleId: "",
      iconFile: "",
      imagePreviewUrl: icon,
      ipaFileUrl: "",
      appFile: "",
      description: "desc",
      screenshots: { img1: "url" },
      timeStamp: undefined,
      lastest_timestamp: undefined,
      device: "iPhone",
      ipaFile: "",
      maniFestPlist: "",
      downloadProgress: 0,
      downloadState: 0,
      uploadFilename: "filename",
      success: false,
      screenshotsFile: undefined,
      screenshotFiles: undefined,
      scrObj: [],
      group_access: [],
      group_access_raw: {}

    }

    this.handleAddIcon = this.handleAddIcon.bind(this)
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleBuildChange = this.handleBuildChange.bind(this)
    this.handleBundleIdChange = this.handleBundleIdChange.bind(this)
    this.handleVersionChange = this.handleVersionChange.bind(this)
    this.submitForm = this.submitForm.bind(this)
    this.handleIpaFile = this.handleIpaFile.bind(this)
    this.handleSaveDataToDB = this.handleSaveDataToDB.bind(this)
    this.saveIpaFileToStorage = this.saveIpaFileToStorage.bind(this)
    this.handleWritePlistFile = this.handleWritePlistFile.bind(this)
    this.saveManiFestPlistToStorage = this.saveManiFestPlistToStorage.bind(this)

  }

  componentDidMount() {
    const uid = auth.getUserCurrent()
    console.log("didmount", uid)
    var ts = this

    db.getUsers().then(function (snapshot) {
      console.log("xcxcxcx", snapshot.val())
      let users = snapshot.val()
      ts.setState({
        uid: uid,
        group_access_raw: users
      })
    })

    // db.getUserData(uid).then(function (userData) {
    //   if (userData.val().groups == null) {

    //   } else {

    //     ts.setState({
    //       uid: uid,
    //       group_access_raw: userData.val().groups
    //     })
    //     console.log("user data group", ts.state.group_access_raw)
    //   }
    // })
  }

  handleWritePlistFile() {
    var thisS = this
    var XMLWriter = require('xml-writer');
    var xw = new XMLWriter();
    xw.startDocument();
    xw.startElement('plist');
    xw.writeAttribute('version', "1.0");
    xw.startElement('dict')
    xw.startElement('key')
    xw.text("items")
    xw.endElement()
    xw.startElement("array")
    xw.startElement('dict')
    xw.startElement('key')
    xw.text("assets")
    xw.endElement()
    xw.startElement('array')
    xw.startElement('dict')
    xw.startElement('key')
    xw.text("kind")
    xw.endElement()
    xw.startElement('string')
    xw.text("software-package")
    xw.endElement()
    xw.startElement('key')
    xw.text("url")
    xw.endElement()
    xw.startElement('string')
    xw.text(thisS.state.ipaFileUrl)   // <--- input url for .ipa file
    xw.endElement()
    xw.endElement()
    xw.endElement()
    xw.startElement('key')
    xw.text("metadata")
    xw.endElement()

    xw.startElement("dict")
    xw.startElement("key")
    xw.text("bundle-identifier")
    xw.endElement()
    xw.startElement("string")
    xw.text(thisS.state.bundleId)  // <--- input bundle id
    xw.endElement()
    xw.startElement("key")
    xw.text("bundle-version")
    xw.endElement()
    xw.startElement("string")
    xw.text(thisS.state.version) // <--- input bundle id
    xw.endElement()
    xw.startElement("key")
    xw.text("kind")
    xw.endElement()
    xw.startElement("string")
    xw.text("software")
    xw.endElement()
    xw.startElement("key")
    xw.text("subtitle")
    xw.endElement()
    xw.startElement("string")
    xw.text("subtitle here") // <--- input subtitle
    xw.endElement()
    xw.startElement("key")
    xw.text("title")
    xw.endElement()
    xw.startElement("string")
    xw.text(thisS.state.name) // <--- input title here
    xw.endElement()
    xw.endDocument();
    thisS.setState({
      maniFestPlist: xw
    })
    this.saveManiFestPlistToStorage()
  }

  saveManiFestPlistToStorage = async () => {
    // SemiFinal step
    var thisS = this
    var manifestFile = thisS.state.maniFestPlist
    var storage = firebase.storage();
    var storageRef = storage.ref();
    var imageRef = storageRef.child(thisS.state.uuid);
    var spaceRef = imageRef.child('ipa/manifest.plist');
    var file = new Blob([manifestFile.toString()], { type: 'application/octet-stream' });

    spaceRef.put(file).then(async (snapshot) => {
      //var downloadURL = snapshot.downloadURL;
      var thissS = thisS

      var longUrl = snapshot.downloadURL


      const API_KEY = "AIzaSyDR3vFkRx8HRc3OkT4y-zj8T2SQFfjfvfw"
      const DYNAMIC_LINK = "mhzw9.app.goo.gl"

      const URL_REQUEST = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`
      const headers = new Headers()
      console.log("QQQQ", URL_REQUEST)
      headers.append('Content-Type', 'application-json')

      const data = {
        "dynamicLinkInfo": {
          "dynamicLinkDomain": DYNAMIC_LINK,
          "link": longUrl
        },
        "suffix": {
          "option": "SHORT"
        }
      }
      const options = {
        method: 'POST',
        headers,
        body: JSON.stringify(data)
      }

      console.log("OPTIONS", options)

      const request = new Request(URL_REQUEST, options)
      const response = await fetch(request)
      const status = await response.status

      if (status == 200) {
        const jsonData = await response.json()
        const shortenUrl = jsonData.shortLink
        thisS.setState({
          appFile: shortenUrl
        })
        console.log("shortURL", )
        this.handleSaveDataToDB()
        console.log("shortURL", shortenUrl)
      }

    })
  }

  submitForm(event) {


    var dbb = firebase.database();
    var newKey = dbb.ref('Applications').push()
    var newK = newKey.key

    var thisS = this
    thisS.setState({
      isUploading: true,
      uuid: newK
    })
    console.log("touch")

    this.handleAddIcons(newK);
    this.saveScreenshotFiles(newK);
    this.saveIpaFileToStorage(newK);
    event.preventDefault();
  }

  handleClearForm(e) {
    e.preventDefault();
  }

  onDescriptionChange = (e) => {
    this.setState({
      description: e.target.value
    })
  }

  handleSaveDataToDB = () => {
    //Final step
    var dbb = firebase.database();
    var group_access_final = this.state.group_access
    console.log("group_access", group_access_final)
    var uuids = this.state.uuid
    console.log("group_access_lenght", group_access_final.length)
    let lenghtPermission = Object.keys(group_access_final)
    if (lenghtPermission.length === 0) {
      group_access_final = { General: true }
    }

    var timeInterval = firebase.database.ServerValue.TIMESTAMP
    var newKey = dbb.ref(`Applications/${uuids}`)
    var uid = this.state.uid

    var userRef = dbb.ref('Users').child(uid)


    newKey.set({
      appId: uuids,
      name: this.state.name,
      fileAppUrl: this.state.appFile,
      appIconUrl: this.state.imagePreviewUrl,
      //group_access: group_access_final,
      permission: group_access_final,
      description: this.state.description,
      developerID: this.state.uid,
      device: this.state.device,
      lastest_timestamp: timeInterval,
      screenshots: this.state.scrObj,
      timeStamp: timeInterval,
      version: this.state.version,
      download: 0,
      build: this.state.build,
      bundleId: this.state.bundleId
    })
    var updateValue = {}
    var keyUpdate = uuids
    updateValue[keyUpdate] = true
    userRef.child("yourApp").update(updateValue)

    this.setState({
      success: true
    })

  }

  handleNameChange(e) {
    this.setState({
      name: e.target.value
    });
  }

  handleVersionChange(e) {
    this.setState({
      version: e.target.value
    });
  }

  handleBuildChange(e) {
    this.setState({
      build: e.target.value
    });
  }

  handleBundleIdChange(e) {
    this.setState({
      bundleId: e.target.value
    });
  }

  saveIpaFileToStorage = (uuids) => {
    var thisS = this
    var storage = firebase.storage();
    var storageRef = storage.ref();
    var imageRef = storageRef.child(uuids);
    console.log("ipa file ccccc", uuids)
    var fileIpaRaw = thisS.state.ipaFile
    var spaceRef = imageRef.child('ipa/app.ipa');
    var metadata = {
      contentType: 'application/octet-stream',
    }

    var x = spaceRef.put(fileIpaRaw, metadata);
    x.on('state_changed', function (snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('ipa file Upload is ' + progress + '% done');
      thisS.setState({
        downloadProgress: progress
      })
    }, function (error) {
      // Handle unsuccessful uploads

    }, function () {
      var downloadURL = x.snapshot.downloadURL;
      thisS.setState({
        ipaFileUrl: downloadURL
      })
      console.log("download", downloadURL)
      thisS.handleWritePlistFile()
    });
  }

  handleIpaFile(e) {
    let file = e.target.files[0];
    this.setState({
      ipaFile: file
    })
  }


  handleScreenshotFile = (e) => {
    if (e.target.files.length > 5) {

      alert("ไฟล์เยอะไปจ้า")
      document.getElementById("screenshot").value = ""
    } else {

      this.setState({
        screenshotFiles: e.target.files
      })
      console.log("screenshot state", this.state.screenshotFiles)
      console.log("screenshot", e.target.files)
      console.log("screenshot", e.target.files[0])
    }
  }

  saveScreenshotFiles = (uuids) => {
    var files = this.state.screenshotFiles



    for (var i = 0; i < files.length; i++) {
      var imageFile = files[i]
      var fileName = `img${i + 1}`
      this.uploadFile(imageFile, fileName, uuids, result => {
        if (result.progress) {
          console.log("progress", result.progress)
          return;
        }

        if (result.downloadURL && result.keyName) {
          console.log("url", result.downloadURL)
          var scrArr = this.state.scrObj
          scrArr[result.keyName] = result.downloadURL

          this.setState({
            scrObj: scrArr
          })

          console.log("scr: ", this.state.scrObj)
          return;
        }

        if (result.error) {

          console.log(`error upload${i}`, result.error)
        }
      });
    }

    console.log("sasas")

  }

  handleGroupChange = () => {
    var checkboxes = document.getElementsByName('group_access[]');
    var vals = []
    console.log("checkboxs", checkboxes)
    for (var i = 0, n = checkboxes.length; i < n; i++) {
      if (checkboxes[i].checked) {
        vals[checkboxes[i].value] = true
        console.log("lenght", vals.length)
        this.setState({
          group_access: vals
        })
        console.log("checked", vals)
      }
    }
  }

  handleDevieChange = (e) => {
    const deviceModel = e.target.value
    console.log("device choose", deviceModel)
    this.setState({
      device: deviceModel
    })
  }

  async uploadFile(file, fileName, uuids, callback) {
    var thiss = this

    var storage = firebase.storage();
    const storageRef = storage.ref();
    console.log("screen shot", uuids)
    const uploadTask = storageRef.child(`${uuids}/screenshots/${fileName}.png`).put(file)

    uploadTask.on('state_changed', snapshot => {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      callback({ progress });
    }, error => {
      callback({ error });
    }, () => {
      var downloadURL = uploadTask.snapshot.downloadURL;
      var keyName = fileName
      callback({ downloadURL, keyName });
    });
  }

  handleAddIcon = (e) => {

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)

  }

  handleAddIcons = (uuids) => {
    console.log("add icon")
    const iconFile = this.state.imagePreviewUrl;
    var thisS = this;
    var storage = firebase.storage();
    var storageRef = storage.ref();
    var imageRef = storageRef.child(uuids);
    var spaceRef = imageRef.child('appIcon/appIcon.png');
    var metadata = {
      contentType: 'image/jpeg',
    };
    var x = spaceRef.putString(iconFile, 'data_url');


    x.on('state_changed', function (snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      thisS.setState({
        downloadProgress: progress
      })

    }, function (error) {

    }, function () {
      var downloadURL = x.snapshot.downloadURL;
      thisS.setState({
        imagePreviewUrl: downloadURL
      })
      console.log("download", downloadURL)
    });
  }


  render() {
    let { imagePreviewUrl } = this.state;
    var usersValue = this.state.group_access_raw
    return (
      <div>
        <h1>Create New Application</h1>
        <div>

          <div className="card-group" style={{ width: '96px', height: '96px' }}>
            <img className="card-img-top" src={imagePreviewUrl} />

          </div>
          <hr />
          <div hidden={this.state.isUploading}>
            <form onSubmit={this.submitForm}>

              <SingleInput
                inputType={'text'}
                title={'Name'}
                name={'name'}
                controlFunc={this.handleNameChange}
                placeholder={'Type name here'}
                required={'true'}
                multipleUpload={true}
              />

              <SingleInput
                inputType={'text'}
                title={'Version'}
                name={'version'}
                controlFunc={this.handleVersionChange}
                placeholder={'Type version here'}
                required={'true'}
                multipleUpload={true}
              />

              <SingleInput
                inputType={'text'}
                title={'Build'}
                name={'build'}
                controlFunc={this.handleBuildChange}
                placeholder={'Type build here'}
                required={'true'}
                multipleUpload={true}
              />

              <SingleInput
                inputType={'text'}
                title={'Bundle Id'}
                name={'bundleId'}
                controlFunc={this.handleBundleIdChange}
                placeholder={'Type bund id here'}
                required={'true'}
                multipleUpload={true}
              />

              <SingleInput
                inputType={'text'}
                title={'Description'}
                name={'descritpion'}
                controlFunc={this.onDescriptionChange}
                placeholder={'typing...'}
                required={'true'}
                multipleUpload={true}
              />

              <SingleInput
                acceptFile={'image/*'}
                inputType={'file'}
                title={'Icon'}
                name={'icon'}
                controlFunc={this.handleAddIcon}
                required={'true'}
                multipleUpload={false}
              />

              <SingleInput
                acceptFile={'.ipa'}
                inputType={'file'}
                title={'.ipa file'}
                name={'fileApp'}
                controlFunc={this.handleIpaFile}
                required={'true'}
                multipleUpload={false}
              />

              <SingleInput
                idInput={'screenshot'}
                acceptFile={'image/*'}
                inputType={'file'}
                title={'Screenshot'}
                name={'screenshot'}
                controlFunc={this.handleScreenshotFile}
                required={'true'}
                multipleUpload={true}
              />

              <div className="form-group">
                <div className="row">
                  <legend className="col-form-label col-sm-2 pt-0">Device</legend>
                  <div className="col-sm-10">
                    <UIRadio
                      idInput={'device'}
                      valueInput={'iPhone'}
                      handleFunc={this.handleDevieChange}
                    />
                    <UIRadio
                      idInput={'device'}
                      valueInput={'iPad'}
                      handleFunc={this.handleDevieChange}
                    />
                    <UIRadio
                      idInput={'device'}
                      valueInput={'Universal'}
                      handleFunc={this.handleDevieChange}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">

                <div className="row">
                  <legend className="col-form-label col-sm-2 pt-0">User Access</legend>
                  <div className="col-sm-10">

                    {
                      Object.keys(this.state.group_access_raw).map(x => {
                        return <UICheckBox
                          key={x}
                          nameInput={'group_access[]'}
                          valueInput={x}
                          displayValue={usersValue[x].name}
                          handleFunc={this.handleGroupChange}
                        />
                      })

                    }
                  </div>
                </div>
              </div>

              <button className="btn btn-primary" disabled={false} >Submit form</button>

            </form>
          </div>
          <ProgressBar
            title={this.state.uploadFilename}
            name={'uploadBar'}
            downloadProgress={this.state.downloadProgress}
          />

          {
            this.state.success === true &&
            <LaunchModal
              title={'Upload process'}
              msg={'Success! >>'}
              urlFile={this.state.appFile}
            />
          }

        </div>
      </div>
    )
  }
}

const authCondition = (authUser) => !!authUser;


export default withAuthorization(authCondition)(CreateNewAppPage);