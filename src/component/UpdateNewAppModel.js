import React, { Component } from 'react';
import withAuthorization from './withAuthorization';
import { auth, db } from '../firebase';
import icon from '../icon.png'
import SingleInput from './SingleInput';
import firebase from 'firebase';
import ProgressBar from './ProgressBar';
import LaunchModal from './LaunchModal';
import { Route, Redirect } from 'react-router';
import { LIST_APP } from '../constants/routes';
import UIRadio from './UIRadio';
import UICheckBox from './UICheckBox';
import PropTypes from 'prop-types';
import SlideShow from './SlideShow'

class UpdateNewAppModel extends Component {
    constructor(props) {
        super(props)

        this.state = {
            appName: undefined,
            appID: undefined,
            appICON: undefined,
            version: undefined,
            applicationItem: undefined,
            isEditing: true,
            description: undefined,
            device: undefined,
            build: undefined,
            bundleID: undefined,
            appFileIPA: undefined,
            isSHUpload: false,
            screenshotFiles: undefined,
            success: undefined,
            downloadProgress: undefined,
            ipaFileUrl: undefined
        }

    }

    handleEditForm = (e) => {
        const targetOfBtn = e.target.value

        if (targetOfBtn == "cancel") {
            this.fetchApplicationData()
            const isEditVal = this.state.isEditing
            this.setState({
                isEditing: !isEditVal
            })
            
        } else {
            const isEditVal = this.state.isEditing
            this.setState({
                isEditing: !isEditVal
            })
        }

    }

    handleNameChange = (e) => {
        const nameVal = e.target.value
        this.setState({
            appName: nameVal
        })
    }

    handleVersionChange = (e) => {
        const versinVal = e.target.value
        this.setState({
            version: versinVal
        })
    }

    handleBuildChange = (e) => {
        const buildVal = e.target.value
        this.setState({
            build: buildVal
        })
    }

    handleBundleIdChange = (e) => {
        const bundleIdVal = e.target.value
        this.setState({
            bundleID: bundleIdVal
        })
    }

    handleDescriptionChange = (e) => {
        const descriptionVal = e.target.value
        this.setState({
            description: descriptionVal
        })
    }

    handleIpaFile = (e) => {
        const ipaFile = e.target.files[0]
        this.setState({
            appFileIPA: ipaFile
        })
    }

    componentDidMount() {
        console.log("APP ID: ", this.props.appID)
        this.fetchApplicationData()
    }

    fetchApplicationData = () => {
        var self = this
        firebase.database().ref('Applications').child(this.props.appID).once('value', (snapshot) => {
            console.log("SNAPSHOT: ", snapshot.val())
            const appData = snapshot.val()
            this.setState({
                appID: this.props.appID,
                appName: appData.name,
                appICON: appData.appIconUrl,
                device: appData.device,
                version: appData.version,
                description: appData.description,
                build: appData.build,
                bundleID: appData.bundleId
            })
        })
    }

    handleWritePlistFile = () => {
        var XMLWriter = require('xml-writer');
        var xw = new XMLWriter();
        xw.startDocument();
        xw.startElement('plist');
        xw.writeAttribute('version', "1.0");
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("items")
        xw.endElement()
        xw.startElement("array")
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("assets")
        xw.endElement()
        xw.startElement('array')
        xw.startElement('dict')
        xw.startElement('key')
        xw.text("kind")
        xw.endElement()
        xw.startElement('string')
        xw.text("software-package")
        xw.endElement()
        xw.startElement('key')
        xw.text("url")
        xw.endElement()
        xw.startElement('string')
        xw.text(this.state.ipaFileUrl)   // <--- input url for .ipa file
        xw.endElement()
        xw.endElement()
        xw.endElement()
        xw.startElement('key')
        xw.text("metadata")
        xw.endElement()
    
        xw.startElement("dict")
        xw.startElement("key")
        xw.text("bundle-identifier")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.bundleId)  // <--- input bundle id
        xw.endElement()
        xw.startElement("key")
        xw.text("bundle-version")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.version) // <--- input bundle id
        xw.endElement()
        xw.startElement("key")
        xw.text("kind")
        xw.endElement()
        xw.startElement("string")
        xw.text("software")
        xw.endElement()
        xw.startElement("key")
        xw.text("subtitle")
        xw.endElement()
        xw.startElement("string")
        xw.text("subtitle here") // <--- input subtitle
        xw.endElement()
        xw.startElement("key")
        xw.text("title")
        xw.endElement()
        xw.startElement("string")
        xw.text(this.state.name) // <--- input title here
        xw.endElement()
        xw.endDocument();
        this.setState({
          maniFestPlist: xw
        })
        this.saveManiFestPlistToStorage()
      }
    
      saveManiFestPlistToStorage = async () => {
        // SemiFinal step
        var thisS = this
        var manifestFile = thisS.state.maniFestPlist
        var storage = firebase.storage();
        var storageRef = storage.ref();
        var imageRef = storageRef.child(thisS.state.appID);
        var spaceRef = imageRef.child('ipa/manifest.plist');
        var file = new Blob([manifestFile.toString()], { type: 'application/octet-stream' });
    
        spaceRef.put(file).then(async (snapshot) => {
          //var downloadURL = snapshot.downloadURL;
          var thissS = thisS
    
          var longUrl = snapshot.downloadURL
    
    
          const API_KEY = "AIzaSyDR3vFkRx8HRc3OkT4y-zj8T2SQFfjfvfw"
          const DYNAMIC_LINK = "mhzw9.app.goo.gl"
    
          const URL_REQUEST = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`
          const headers = new Headers()
          console.log("QQQQ", URL_REQUEST)
          headers.append('Content-Type', 'application-json')
    
          const data = {
            "dynamicLinkInfo": {
              "dynamicLinkDomain": DYNAMIC_LINK,
              "link": longUrl
            },
            "suffix": {
              "option": "SHORT"
            }
          }
          const options = {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
          }
    
          console.log("OPTIONS", options)
    
          const request = new Request(URL_REQUEST, options)
          const response = await fetch(request)
          const status = await response.status
    
          if (status == 200) {
            const jsonData = await response.json()
            const shortenUrl = jsonData.shortLink
            thisS.setState({
              appFile: shortenUrl
            })
            console.log("shortURL", )
            this.handleSaveDataToDB()
            console.log("shortURL", shortenUrl)
          }
    
        })
      }

      saveIpaFileToStorage = () => {
        var thisS = this
        var storage = firebase.storage();
        var storageRef = storage.ref();
        var uuids = "ssss"//this.state.appID
        var imageRef = storageRef.child(uuids);
        console.log("ipa file ccccc", uuids)
        var fileIpaRaw = this.state.appFileIPA
        var storageRef = imageRef.child('ipa/app.ipa');
        var metadata = {
          contentType: 'application/octet-stream',
        }

        var uploadFile = storageRef.put(fileIpaRaw)
    
        uploadFile.on('state_changed', function(snapshot){
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
          }, function(error) {
              console.log("upload error", error)
            // Handle unsuccessful uploads
          }, function() {
            var downloadURL = uploadFile.snapshot.downloadURL;
            console.log("downURL", downloadURL)
          });

      }

      handleSaveDataToDB = () => {
        //Final step
        var dbb = firebase.database();
        var uuids = this.state.appID;
        var timeInterval = firebase.database.ServerValue.TIMESTAMP
        var newKey = dbb.ref(`Applications/${uuids}`)
    
    
        newKey.update({
                name: this.state.name,
                fileAppUrl: this.state.appFile,
                description: this.state.description,
                lastest_timestamp: timeInterval,
                version: this.state.version,
                build: this.state.build,
                bundleId: this.state.bundleId
              
        })
    
        this.setState({
          success: true
        })

        console.log("complete")
    
      }
    
    
    submitForm = () => {
        this.saveIpaFileToStorage()
        console.log("submitform")
    }

    render() {

        var appIcon = this.state.appICON
        console.log("VOVOVO", appIcon)
        //var appIcon = applicationModel.appIconUrl
        return (
            <div>
                <h1>Update Application</h1>
                <hr />
                <div>
                    <button onClick={this.handleEditForm} value="update" className="btn btn-warning" hidden={!this.state.isEditing}>Update</button>&nbsp;
                    <button onClick={this.handleEditForm} value="cancel" className="btn btn-warning" hidden={this.state.isEditing}>Cancel</button>&nbsp;
                    <button className="btn btn-success" onClick={this.props.handleFunc} >Back</button>
                    <hr />
                    <div className="card-group" style={{ width: '96px', height: '96px' }}>
                        <img className="card-img-top" src={appIcon} />
                    </div>
                    <hr />
                    <div hidden={this.state.isUploading}>
                        <form onSubmit={this.submitForm}>

                            <SingleInput
                                valueInput={this.state.appName}
                                inputType={'text'}
                                title={'Name'}
                                name={'name'}
                                controlFunc={this.handleNameChange}
                                placeholder={'Type name here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.version}
                                inputType={'text'}
                                title={'Version'}
                                name={'version'}
                                controlFunc={this.handleVersionChange}
                                placeholder={'Type version here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.build}
                                inputType={'text'}
                                title={'Build'}
                                name={'build'}
                                controlFunc={this.handleBuildChange}
                                placeholder={'Type build here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.bundleID}
                                inputType={'text'}
                                title={'Bundle Id'}
                                name={'bundleId'}
                                controlFunc={this.handleBundleIdChange}
                                placeholder={'Type bund id here'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                valueInput={this.state.description}
                                inputType={'text'}
                                title={'Description'}
                                name={'descritpion'}
                                controlFunc={this.handleDescriptionChange}
                                placeholder={'typing...'}
                                required={'true'}
                                multipleUpload={true}
                                disabledInput={this.state.isEditing}
                            />

                            <SingleInput
                                hiddenInput={this.state.isEditing}
                                acceptFile={'.ipa'}
                                inputType={'file'}
                                title={'.ipa file'}
                                name={'fileApp'}
                                controlFunc={this.handleIpaFile}

                                multipleUpload={false}
                                disabledInput={this.state.isEditing}
                            />

                            <button className="btn btn-primary" hidden={this.state.isEditing} disabled={false} >Submit form</button>

                        </form>
                    </div>
                    <div hidden={this.state.isEditing}>
                        <ProgressBar
                            title={this.state.uploadFilename}
                            name={'uploadBar'}
                            downloadProgress={this.state.downloadProgress}
                        />

                        {
                            this.state.success == true &&
                            <LaunchModal
                                hidden={this.state.isEditing}
                                title={'Upload process'}
                                msg={'Success! >>' + this.state.appFile}
                            />
                        }
                    </div>
                </div>
            </div>
        )
    };

}

export default UpdateNewAppModel
