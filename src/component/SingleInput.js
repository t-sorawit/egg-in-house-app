import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SingleInput extends React.Component {


  render() {
    return (
      <div className="form-group">
        <label className="form-label" hidden={this.props.hiddenInput}>{this.props.title}</label>
        <input
          value={this.props.valueInput}
          id={this.props.idInput}
          accept={this.props.acceptFile}
          className="form-control"
          name={this.props.name}
          type={this.props.inputType}
          onChange={this.props.controlFunc}
          placeholder={this.props.placeholder}
          required={this.props.required}
          multiple={this.props.multipleUpload}
          disabled={this.props.disabledInput}
          hidden={this.props.hiddenInput}
        />
      </div>
    )
  }

}


SingleInput.propTypes = {
  
  acceptFile: PropTypes.string,
  inputType: PropTypes.oneOf(['text', 'file']),
  title: PropTypes.string,
  name: PropTypes.string,
  controlFunc: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.string,
  multipleUpload: PropTypes.bool,
  idInput: PropTypes.string,
  valueInput: PropTypes.string,
  disabledInput: PropTypes.bool,
  hiddenInput: PropTypes.bool
};

export default SingleInput;  