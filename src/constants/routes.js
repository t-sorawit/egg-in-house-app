export const SIGN_IN = '/signin';
export const CREATE_NEW_APP = '/newApp';
export const HOME = '/';
export const LIST_APP = '/listApp';
export const UPDATE_APP = '/updateApp';
