import { db } from './firebase';

export const doCreateUser = (id, username, email) =>
  db.ref('users/${id}').set({
    username,
    email,
  })

export const getUsers = () =>
  db.ref(`Users`).once('value')

export const getApplications = () =>
  db.ref('Applications/').once('value')

export const getApplicationsByName = (name) =>
 db.ref(`Applications/${name}`).once('value')

export const getUserData = (uid) =>
  db.ref(`Users/${uid}`).once('value')

export const getGroupAccess = (uid) =>
  db.ref(`Users/${uid}/groups`).once('value')

export const onceGetUsers = (uid) =>
  db.ref(`Users/${uid}`).once('value')

export const deleteApplication = (appID) =>
  db.ref(`Applications/${appID}`).remove()

  export const deleteOurApplication = (appID, uid) =>
    db.ref(`Users/${uid}/yourApp/${appID}`).remove()

export const editApplication = (applicationObj) => 
  db.ref(`Applications/${applicationObj.name}`).set(applicationObj)

export const editApplicationByName = (name) =>
  db.ref(`Applications/${name}`).set({name: "newApp", version: "1.0.3"})